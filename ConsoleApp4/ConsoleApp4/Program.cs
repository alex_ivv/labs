﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            double y;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int x = -10; x <= 10; x++)
            {
                if (x > 0) 
                {
                    Console.Write("Y при X равном {0} :  ",x);
                    y = 2 * x - 10;
                }
                else if (x == 0)
                {
                    Console.Write("Y при X равном {0} :  ", x);
                    y = 0;
                }
                else
                {
                    Console.Write("Y при X равном {0} :  ", x);
                    y = 2 * Math.Abs(x) - 1;
                }

                Console.WriteLine(y);
            }
            sw.Stop();
            Console.WriteLine("Затраченное время : {0}", sw.Elapsed);
            Console.ReadKey();
        }
    }
}
