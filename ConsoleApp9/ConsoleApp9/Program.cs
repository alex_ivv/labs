﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            int i;
            DateTime g,d;
            
            Console.Write("Введите число : ");
            i = int.Parse(Console.ReadLine());
            sw.Start();

            d = DateTime.Now;
            g = d.AddDays(i);

            Console.WriteLine("Через {0} дн(ей/я) будет : {1}",i,g);
            sw.Stop();
            Console.WriteLine("Затраченное время : {0}",sw.Elapsed);
            Console.ReadKey();
            
        }
    }
}
