﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pusir
{
    class Program
    {
        static void FillArray(int[] array,Random rnd)
        {
            for (int i = 0; i < 10; i++)
            {
                array[i] = rnd.Next(20);
            }
        }

        static void Sort(int[] array)
        {
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9 - i; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        int buf = array[j + 1];
                        array[j + 1] = array[j];
                        array[j] = buf;
                    }
                }

            }
        }

        static void WriteArray(int[] array)
        {
            for (int i = 0; i < 10; i++)
                Console.WriteLine(array[i]);
        }

        static void Main(string[] args)
        {
            int[] array = new int[10];
            Random rnd = new Random();

            FillArray(array, rnd);
            WriteArray(array);
            
            Console.WriteLine("__________________________________");

            Sort(array);
            
            WriteArray(array);
            Console.ReadKey();
        }
    }
}
