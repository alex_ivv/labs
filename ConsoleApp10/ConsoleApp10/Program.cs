﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp10
{
    class Program
    {
        static void Main(string[] args)
        {
            const int n = 10;
            int[,] array = new int[n, n+5];
            Random rnd = new Random();

            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++) 
                {
                    array[i, j] = rnd.Next(20);
                }
            }
            int[] sum = new int[10];
            
            for (int i = 0; i < array.GetLength(0); i++)
            {
                sum[i] = 0;
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    Console.Write("{0:00} ",array[i, j]);
                    sum[i] = sum[i] + array[i, j];
                }

                Console.WriteLine("Сумма : {0} ",sum[i]);

            }
            Console.ReadKey();
        }
    }
}
