﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ryad2
{
    class Program
    {
        static void Main(string[] args)
        {
            int x,st;
            int n = 0;
            double ex = 0;

            Console.WriteLine("Ряд e^-x");
            Console.Write("Введите x : ");
            x = int.Parse(Console.ReadLine());
            Console.Write("До какой степени разложить ряд? : ");
            st = int.Parse(Console.ReadLine());

            do
            {

                int fact = 1;
                for (int i = 1; i <= n; i++)
                {
                    if (i != 0)
                        fact = i * fact;
                }

                ex = ex + (Math.Pow(-1, n) * Math.Pow(x, n)) / fact; 

                n++;

            } while (n < st);

            Console.WriteLine("Сумма ряда = {0:0.00000}",ex);
            Console.ReadKey();
        }
    }
}
