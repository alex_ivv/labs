﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example1
{
    class Program
    {

        static void CopyArray(int[] from, int[] to)
        {
            for(int i = 0; i < from.Length; i++)
            {
                to[i] = from[i];
            }
        }

        static int[] CreateArray(int n, Random rnd)
        {
            int[] array = new int[n];
            FillArray(array,rnd);
            return array;
            
        }

        static void FillArray(int[] array,Random rnd)
        {
            
            for (int i = 0; i < 10; i++)
            {
                array[i] = rnd.Next(20);
            }
        }

        static void WriteArray(int[] array)
        {
            for (int i = 0; i < 10; i++)
            {
                Console.Write($"{array[i]} ");
            }
        }

        static int Main(string[] args)
        {
            int a, b;
            a = int.Parse(Console.ReadLine());
            b = a;
            a = int.Parse(Console.ReadLine());

            Random rnd = new Random();
            Console.WriteLine($"{a} {b}");
            Console.ReadKey();

            int[] c, d = new int[10];
            c = CreateArray(10,rnd);

            CopyArray(c, d);
            FillArray(c,rnd);

            WriteArray(c);
            Console.WriteLine();
            WriteArray(d);

            Console.ReadKey();
            return 0;
        }
    }
}
