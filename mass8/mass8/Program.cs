﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mass8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите длину массива : ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine();
            int[] array = new int[n];
            Random rnd = new Random();

            for (int i = 0; i < n; i++)
            {
                array[i] = rnd.Next(-20,20);
                Console.Write(" {0}",array[i]);
            }
            Console.WriteLine();
            Console.WriteLine("------------------------------------------------------------------------");


            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - 1 - i; j++)
                {
                    if (Math.Abs(array[j]) > Math.Abs(array[j + 1]))
                    {
                        int buf = array[j + 1];
                        array[j + 1] = array[j];
                        array[j] = buf;
                    }
                }

            }

            for (int i = 0; i < n; i++)
                Console.Write(" {0}",array[i]);
            Console.ReadKey();
        }
    }
}
