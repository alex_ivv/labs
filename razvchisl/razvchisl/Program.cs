﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace razvchisl
{
    class Program
    {
        static int Func(int i, int t)
        {
            if (i % 10 == i)
            {
                return i * (int)Math.Pow(10, t);
            }
            
            return Func(i % 10,t) + Func(i / 10, t)/10;
        }

        static int Func2(int i,int t)
        {
            do
            {
                i = i / 10;
                t++;

            } while (i / 10 > 0);
            return t; 
        }

        static void Main(string[] args)
        {
            int i,t = 0;
            
            Console.WriteLine("Введите i ");
            i = int.Parse(Console.ReadLine());
            Console.WriteLine(Func(i,Func2(i,t)));
            Console.ReadKey();
        }
    }
}
