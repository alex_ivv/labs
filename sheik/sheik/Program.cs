﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sheik
{
    class Program
    {
        static void FillArray(int[] array, Random rnd)
        {
            for (int i = 0; i < 10; i++)
            {
                array[i] = rnd.Next(20);
            }
        }

        static void WriteArray(int[] array)
        {
            for (int i = 0; i < 10; i++)
                Console.WriteLine(array[i]);
        }

        static void Sort(int[] array)
        {
            int left = 0;
            int right = 9;

            do
            {
                for (int j = left; j < right; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        int buf = array[j + 1];
                        array[j + 1] = array[j];
                        array[j] = buf;
                    }
                }
                right--;
                for (int k = right; k > left; k--)
                {
                    if (array[k] < array[k - 1])
                    {
                        int buf = array[k - 1];
                        array[k - 1] = array[k];
                        array[k] = buf;
                    }
                }
                left++;
            } while (right - left > 1);
        }

        static void Main(string[] args)
        {
            int[] array = new int[10];
            Random rnd = new Random();

            FillArray(array, rnd);
            WriteArray(array);    

            Console.WriteLine("__________________________________");

            Sort(array);
            WriteArray(array);
            Console.ReadKey();
        }
    }
}
