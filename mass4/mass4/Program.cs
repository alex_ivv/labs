﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mass4
{
    class Program
    {
        static void Main(string[] args)
        {
            int pos;

            Console.Write("Введите длину массива : ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];

            Random rnd = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(-20, 20);
            }

            pos = 0;
            for (int j = 0; j < array.Length; j++)
            {
                if (array[j] > 0)
                    pos = j;
                Console.WriteLine(array[j]);
            }

            int sum = 0;
            
            for(int k = 0; k <= pos; k++)
            {
                sum = sum + array[k];
            }


            Console.WriteLine("Сумма элементов от 1 до {0} равна : {1}  ", pos+1, sum);
            Console.ReadKey();
        }
    }
}
