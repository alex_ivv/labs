﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    
    public partial class Form1 : Form
    {
        private bool n;
        int v;

        public Form1()
        {
            InitializeComponent();
        }

        static int Rand(string[] g, int v)
        {
            Random rnd = new Random();
            if (v == 0)
                return rnd.Next(g.Length);

            else if (v == 1)
                return rnd.Next(1, 101);

            else
                return -1;
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {
            v = 0;
            if (n == true)
            {
                string[] mesto = { "То Место", "Хайп", "Флоатинг",
                          "Массаж", "Термы", "Кинза", "Эль Президент",
                          "Красная дверь", "ВО", "Аями", "Дело в Рисе", "Хэлло", "Сплетни",
                          "Кино", "Театр", "Аттракционы", "Спа программа", "Казань", "Додо",
                          "Мама Пицца", "Нино играет в домино", "Бирюза", "Муму", "Ронни",
                          "Так или Начос", "Пентхаус", "Торнадо", "Цирк", "Теремок", "Бар Бургер", "Ботаник Сфера",
                          "Огни", "Гравити парк", "Фотосессия", "Катание на лошадях", "Оленья ферма",
                          "Вчерашние новости", "Хаски", "Коньки или ролики", "Виртуальная реальность", 
                          "Квесты", "Колокол", "Пряный кролик", "Луна и черепаха", "Концепт", "Катание на катамаранах", 
                          "Купить парную одежду", "Тату", "Подобрать новый образ", "Чайхана" };


                richTextBox2.Text = string.Format(" {0}", mesto[Rand(mesto, v)]);
            }
            if (n == false)
            {
                string[] nomoney = { "Прогулка по парку", "Романтический ужин", "Выезд на природу", "Настолки",
                          "Кофе вдвоем", "Набережная", "Фотосессия", "Рисование", "Смастерить что-нибудь", "Съездить к родителям",
                          "Пикник", "Генуборка", "Спа вечер", "Кальян на природе", "Выставка", "Сходить в гости", "Позвать в гости",
                          "Прогулка по городу", "Подарки своими руками", "Сауна", "Танец", "Тематический вечер", "Пробежка", "Перебрать вещи", 
                          "Зоопарк", "Домик своими руками", "Гастро соревнование", "Йога", "Велосипеды" };

                richTextBox2.Text = string.Format(" {0}", nomoney[Rand(nomoney, v)]);
            }
        }

     
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            v = 0;
            string[] kak = { "", "Связать его", "Закрытые глаза", "Розовая",
                            "Черная", "Жестко", "Нежно", "В публичном месте",
                            "Смотреть на него", "Смотреть на нее", "Своими руками", "Двойное с черной", 
                            "Любые его желания", "69" ,"В душе" ,"Минет" ,"Куни", "Двойное с розовой", "Любые ее желания",
                            "Связать ее", "Доминировать над ней", "Доминировать над ним", "Игра" };

            int Indx = Rand(kak, v);


            if (Indx == 0)
            {
                v = 1;
                richTextBox1.Text = string.Format("");
                LinkLabel link = new LinkLabel();
                link.Text = "Поза" + " " + Rand(kak,v);
                link.LinkClicked += new LinkLabelLinkClickedEventHandler(this.link_LinkClicked);
                LinkLabel.Link data = new LinkLabel.Link();
                data.LinkData = "https://brodude.ru/100-poz-iz-kamasutry/";
                link.Links.Add(data);
                link.AutoSize = true;
                link.Location = this.richTextBox1.GetPositionFromCharIndex(this.richTextBox1.TextLength);
                link.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
                link.LinkColor = System.Drawing.Color.Black;
                link.ActiveLinkColor = System.Drawing.Color.Blue;
                this.richTextBox1.Controls.Add(link);
                this.richTextBox1.AppendText(link.Text + " ");
                this.richTextBox1.SelectionStart = this.richTextBox1.TextLength;
            }

            else
            {
                this.richTextBox1.Controls.Clear();
                richTextBox1.Text = string.Format(" {0} ", kak[Indx]);
            }
            
        }
        private void link_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Link.LinkData.ToString());
        }
        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = button1.Text;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            n = true;
            richTextBox2.Text = button2.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
           n = false;
           richTextBox2.Text =  button3.Text;
        }
    } 
}
