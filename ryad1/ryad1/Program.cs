﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ryad1
{
    class Program
    {
        static void Main(string[] args)
        {
            double cosx = 0;
            int x,st;
            int n = 0;
               
            Console.WriteLine("Сумма ряда CosX");
            Console.Write("Введите x : ");
            x = int.Parse(Console.ReadLine());
            Console.Write("До какой степени разложить ряд? : ");
            st = int.Parse(Console.ReadLine());

            do
            {

                int fact = 1;
                for (int i = 1; i <= 2*n; i++)
                {
                    if (i != 0)
                        fact = i * fact; 
                }

                cosx = cosx + (Math.Pow(-1, n) * Math.Pow(x, 2 * n)) / fact;
                
                n++;

            } while (n < st);

            Console.WriteLine("Сумма ряда = {0:0.00000}",cosx);
            Console.ReadKey();
        }
    }
}
 