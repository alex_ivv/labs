﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            int maxi;
            int[] array = new int[10];
            Random rnd = new Random();

            for (int i = 0; i < 10; i++)
            {
                array[i] = rnd.Next(20);
                Console.WriteLine(array[i]);
                
            }
            Console.WriteLine("__________________________________");
            for (int i = 0; i < 10; i++)
            {
                maxi = i;
                for (int j = i; j < 10; j++)
                {
                    if (array[j] > array[maxi])
                    {
                        maxi = j;
                    }
                }
                int buf = array[i];
                array[i] = array[maxi];
                array[maxi] = buf;
            }
            for (int i = 0; i < 10; i++)
                Console.WriteLine(array[i]);
            Console.ReadKey();
        }
    }
}
