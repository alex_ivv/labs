﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp12
{
    class A
    {
        public virtual void Print()
        {
            Console.WriteLine("A");
        }
    }

    class B:A
    {
        public override void Print()
        {
            Console.WriteLine("B");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            A a = new A();
            A b = new B();
            a.Print();
            b.Print();

            Console.ReadKey();
        }
    }
}
