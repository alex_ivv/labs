﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fract
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Fractions a = new Fractions(72, 1);
                Fractions b = new Fractions(128, 72);
                Console.WriteLine(a+b);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }
    }
}
