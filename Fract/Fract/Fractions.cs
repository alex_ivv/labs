﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fract
{
    public class Fractions
    {
        public int Num { get; set; }
        public int Denom { get; set; }

        static int Sokr(int Num, int Denom)
        {
            if (Num > Denom)
                return Sokr(Num - Denom, Denom);

            if (Num < Denom)
                return Sokr(Num, Denom - Num);
            else
                return Num;
        }

        public Fractions()
        {
            Num = 1;
            Denom = 1;
        }

        public Fractions(int num, int denom)
        {
            if (denom == 0)
                throw new ArgumentException();

            Num = num / Sokr(num, denom);
            Denom = denom / Sokr(num, denom);
            
        }

        public Fractions(Fractions r)
        {
            Num = r.Num;
            Denom = r.Denom;
        }

        public static Fractions operator +(Fractions a, Fractions b)
        {
            return new Fractions(a.Num * b.Denom + b.Num * a.Denom, a.Denom * b.Denom);
        }
        public static Fractions operator +(Fractions a, int d)
        {
            return new Fractions(a.Num + d * a.Denom, a.Denom);
        }
        public static Fractions operator +(int d, Fractions a)
        {
            return new Fractions(a.Num + d * a.Denom, a.Denom);
        }

        public static Fractions operator -(Fractions a, Fractions b)
        {
            return new Fractions(a.Num * b.Denom - b.Num * a.Denom, a.Denom * b.Denom);
        }
        public static Fractions operator -(Fractions a, int d)
        {
            return new Fractions(a.Num - d * a.Denom, a.Denom);
        }
        public static Fractions operator -(int d, Fractions a)
        {
            return new Fractions(d * a.Denom - a.Num, a.Denom);
        }

        public static Fractions operator *(Fractions a, Fractions b)
        {
            return new Fractions(a.Num * b.Num, a.Denom * b.Denom);
        }
        public static Fractions operator *(Fractions a, int d)
        {
            return new Fractions(a.Num * d, a.Denom);
        }
        public static Fractions operator *(int d, Fractions a)
        {
            return new Fractions(a.Num * d, a.Denom);
        }

        public static Fractions operator /(Fractions a, Fractions b)
        {
            return new Fractions(a.Num * b.Denom, a.Denom * b.Num);
        }
        public static Fractions operator /(Fractions a, int d)
        {
            return new Fractions(a.Num, a.Denom * d);
        }
        public static Fractions operator /(int d, Fractions a)
        {
            return new Fractions(d * a.Denom, a.Num);
        }

        public override string ToString()
        {
            if (Num == Denom)
                return $"1";

            if (Denom == 1)
                return $"{Num}";

            return $"{Num} / {Denom}";
        }
    }
}
