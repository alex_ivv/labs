﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C
{
    class Program
    {
        static int Func(int m, int n)
        {
            if (m < 0 || n < 0)
            {
                Console.WriteLine("Посчитаны положительные числа");
                return Func(Math.Abs(m), Math.Abs(n));
            }

            if (m == 0)
                return n + 1;

            else if (n == 0)
                return Func(m - 1, 1);

            else
                return Func(m - 1, Func(m, n - 1));
        }

        static void Main(string[] args)
        {
            int m, n;

            Console.Write("Введите m : ");
            m = int.Parse(Console.ReadLine());
            Console.Write("Введите n : ");
            n = int.Parse(Console.ReadLine());
            
            Func(m, n);

            Console.Write(Func(m, n));
            Console.ReadKey();
        }
    }
}
