﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp11
{
    public abstract class Vehicle
    {
        static public ConsoleColor color;
        public int PassengersCount { get; set; }

        public virtual void Move()
        {

        }

        public void Stop()
        {

        }
    }
}
