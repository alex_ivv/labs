﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp11
{
    public class Person
    {
        private DateTime birthDate;
        private string name;

        public int Age { get; set; }

        public int GetAge()
        {
            return DateTime.Now.Year - birthDate.Year;
        }

        public void SetDate(DateTime dateTime)
        {
            birthDate = dateTime;
        }

        public void Display()
        {
            Console.WriteLine($"Dog's name: {name}");
            Console.WriteLine($"Dog's age: {age}");
        }
    }
}
