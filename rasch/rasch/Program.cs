﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rasch
{
    class Program
    {
        static void WriteArray(int[] array, int n)
        {
            for (int i = 0; i < n; i++)
                Console.WriteLine($"{array[i],3}");
        }

        static void FillArray(int[] array, Random rnd,int n)
        {
            for (int i = 0; i < n; i++)
            {
                array[i] = rnd.Next(-20,20);
            }
        }

        

        static void Sort(int[] array, double l, double c, int n)
        {
            int i = 0;
            
            do
            {
                int j = 0;

                do
                {
                    if (array[j] > array[j + (int)l])
                    {
                        int buf = array[j + (int)l];
                        array[j + (int)l] = array[j];
                        array[j] = buf;
                    }
                    j++;

                } while (j + (int)l <= n - 1);

                l = l / c;
                i++;

            } while ( i < n - 1);
        }

        static void Main(string[] args)
        {
            const double c = 1.247;
            
            Console.Write("Введите длину массива : ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];

            Random rnd = new Random();
            
            FillArray(array, rnd, n);
            WriteArray(array, n);
            
            Console.WriteLine("__________________________________");

            double l = n / c;
            Sort(array, l, c, n);
            
            WriteArray(array, n);
            Console.ReadKey();
        }
    }
}
