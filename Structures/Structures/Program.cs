﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structures
{
    class Program
    {
        static void Main(string[] args)
        {
            SuperList s = new SuperList();
            s.Add(4);
            s.Add(3);
            s.Add(5);

            Console.WriteLine($"{s.Get(0)}");
            Console.WriteLine($"{s.Get(1)}");
            Console.WriteLine($"{s.Get(2)}");

            //List<int> list = new List<int>();
            //Random rnd = new Random();

            //for(int i = 0; i < 10; i++)
            //{
            //    list.Add(rnd.Next(10));
            //}

            //for (int i = 0; i < 10; i++)
            //{
            //    Console.Write($"{list[i]} ");
            //}

            //Console.WriteLine();
            //Console.WriteLine("-------------------------");

            //Stack<int> stack = new Stack<int>();


            //for (int i = 0; i < 10; i++)
            //{
            //    int i1 = rnd.Next(10);
            //    stack.Push(i1);
            //    Console.Write($"{i1} ");
            //}

            //Console.WriteLine();
            //for (int i = 0; i < 10; i++)
            //{
            //    Console.Write($"{stack.Pop()} ");                
            //}

            //Console.WriteLine();
            //Console.WriteLine("-------------------------");

            //Queue<int> queue = new Queue<int>();

            //for (int i = 0; i < 10; i++)
            //{
            //    int i1 = rnd.Next(10);
            //    queue.Enqueue(i1);
            //    Console.Write($"{i1} ");
            //}

            //Console.WriteLine();
            //for (int i = 0; i < 10; i++)
            //{
            //    Console.Write($"{queue.Dequeue()} ");
            //}

            //Console.WriteLine();
            //Console.WriteLine("-------------------------");

            //Dictionary<string, int> dict = new Dictionary<string, int>();
            ///*
            //dict.Add("first", 1);
            //dict["second"] = 2;

            //foreach(string s in dict.Keys)
            //{
            //    Console.WriteLine($"{s} : {dict[s]}");
            //}
            //*/
            //string[] arr = new string[3];
            //arr[0] = "first";
            //arr[1] = "second";
            //arr[2] = "third";

            //for (int i = 0; i < arr.Length; i++)
            //{
            //    dict[arr[i]] = rnd.Next(10);
            //}

            //foreach(string s in dict.Keys)
            //{

            //    Console.WriteLine($"{s} : {dict[s]}");
            //}
            Console.ReadKey();
        }
    }
}
