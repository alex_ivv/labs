﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structures
{
    public class SuperList
    {
        public Node Head { get; set; }

        public SuperList()
        {
            Head = new Node(0);
        }

        public void Add(int elem)
        {
            Node current = Head;
            while (current.Next != null)
                current = current.Next;

            current.Next = new Node(elem);
        }

        public int Get(int index)
        {
            Node current = Head.Next;
            while (index != 0)
            {
                index--;
                current = current.Next;
            }

            return current.Data;
        }

        public void Remove(int index)
        {

        }
    }
}
