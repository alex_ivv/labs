﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace b
{
    class Program
    {
        static void Vivod(int a, int b)
        {
            if (a == b)
            {
                Console.Write($"{a} ");
                return;
            }
            if(a > b)
            {
                Console.Write($"{a} ");
                Vivod(a - 1,b);
            }
            else if(b > a)
            {
                Vivod(a, b - 1);
                Console.Write($"{b} ");
            }
            
        }

        static void Main(string[] args)
        {
            int a, b;

            Console.Write("Введите a : ");
            a = int.Parse(Console.ReadLine());
            Console.Write("Введите b : ");
            b = int.Parse(Console.ReadLine());

            Vivod(a,b);
            
            Console.ReadKey();
        }
    }
}
