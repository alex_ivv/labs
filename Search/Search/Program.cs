﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Search
{
    class Program
    {
        static int Search1(int[] array, int s, int a, int b )
        {
            int c = a + (b - a) / 2;

            if (s == array[c])
            {
                return c;
            }
            else if (b == a)
            {
                return -1;
            }
            else if (s > array[c])
            {
                a = c+1;
                return Search1(array, s, a, b);
            }
            else
            {
                b = c-1;
                return Search1(array, s, a, b);
            }

        }

        static int IterativeSearch(int[] arr, int s, int l, int r)
        {
            while (l != r)
            {
                int m = l + (r - l) / 2;
                if (arr[m] == s)
                    return m;
                else if (arr[m] > s)
                    r = m - 1;
                else l = m + 1;
            }
            if (arr[l] == s)
                return l;
            return -1;
        }

        static void Main(string[] args)
        {
            int s;
            s = int.Parse(Console.ReadLine());
            int[] array = new int[] { 1, 3, 4, 7, 8, 13, 25 };
            int a = 0;
            int b = array.Length - 1;
            Console.WriteLine(IterativeSearch(array, s, a, b));
            Console.ReadKey();
        }
    }
}
