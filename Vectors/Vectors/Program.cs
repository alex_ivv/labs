﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vectors;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Vector a = new Vector(5, 3, 4);
            Vector b = new Vector(1, 2, 2);
            Vector c = a * b;
            int i = a.GetHashCode();
            Console.WriteLine($"{c}");
            Console.ReadKey();
        }
    }
}
