﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vectors
{
    public class Vector
    {
        private static int counter;
        private int id;
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public Vector()
        {
            id = counter++;
            X = 1;
            Y = 1;
            Z = 1;
        }

        public Vector(double x, double y, double z)
        {
            id = counter++;
            X = x;
            Y = y;
            Z = z;
        }

        public static Vector operator +(Vector a, Vector b)
        {
            return new Vector(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static Vector operator -(Vector a, Vector b)
        {
            return new Vector(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        public static Vector operator *(Vector a, double d)
        {
            return new Vector(a.X * d, a.Y * d, a.Z * d);
        }

        public static Vector operator *(double d, Vector a)
        {
            return new Vector(a.X * d, a.Y * d, a.Z * d);
        }

        public static Vector operator *(Vector a, Vector b)
        {
            return new Vector(a.Y * b.Z - a.Z * b.Y, a.Z * b.X - a.X * b.Z, a.X * b.Y - a.Y * b.X);
        }

        public override bool Equals(object obj)
        {
            Vector vector = (Vector)obj;
            if (vector == null)
                return false;

            return X == vector.X && Y == vector.Y;
        }

        public override string ToString()
        {
            return $"({X},{Y},{Z})";
        }

        public override int GetHashCode()
        {
            return id;
        }
    }
}
