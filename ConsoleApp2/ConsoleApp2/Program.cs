﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b, c;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Console.WriteLine("Введите первое число :");
            a = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите второе число :");
            b = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите третье число :");
            c = double.Parse(Console.ReadLine());

            if (a > b && a < c || a > c && a < b)
            {
                Console.WriteLine("Среднее число : {0}", a);
            }
            else if (b > a && b < c || b < a && b > c)
            {
                Console.WriteLine("Среднее число : {0}", b);
            }
            else if (c > a && c < b || c < a && c > b)
            {
                Console.WriteLine("Среднее число : {0}", c);
            }
            else
            {
                Console.WriteLine("Среднего числа нет");
            }
            sw.Stop();
            Console.WriteLine("Затраченное время : {0}", sw.Elapsed);
            Console.ReadKey();
        }
    }
}
