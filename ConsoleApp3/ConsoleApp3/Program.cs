﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Console.WriteLine("Введите координату X");
            a = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите координату Y");
            b = double.Parse(Console.ReadLine());

            if (a > 0 && b > 0) 
            {
                Console.WriteLine("Точка ({0},{1}) находится в I плоскости координат ",a,b);
            }
            else if(a < 0 && b > 0) 
            {
                Console.WriteLine("Точка ({0},{1}) находится во II плоскости координат ", a, b);
            }
            else if (a < 0 && b < 0) 
            {
                Console.WriteLine("Точка ({0},{1}) находится в III плоскости координат ", a, b);
            }
            else if (a > 0 && b < 0)
            {
                Console.WriteLine("Точка ({0},{1}) находится в IV плоскости координат ", a, b);
            }
            sw.Stop();
            Console.WriteLine("Затраченное время : {0}", sw.Elapsed);
            Console.ReadKey();
        }
    }
}
