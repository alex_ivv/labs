﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickSort
{
    class Program
    {
        static void QS(int[] arr ,int frst , int scnd)
        {
            int c = (frst + scnd) / 2;

            int i = frst;
            int j = scnd;
            
            while (i <= j)
            {
                while (arr[i] < arr[c]) i++;
                while (arr[j] > arr[c]) j--;

                if(i <= j)
                {
                    int buff = arr[i];
                    arr[i] = arr[j];
                    arr[j] = buff;

                    i++;
                    j--;
                }
            }
            if (j > frst) QS(arr, frst, j);
            if (i < scnd) QS(arr, i, scnd);


            /*if (arr[c] > arr[frst] && frst <= c)
            {
                buff = arr[frst];
                arr[frst] = arr[c];
                arr[c] = buff;
                frst++;
                QS(arr, frst, scnd);
            }
            
            else if (arr[c] < arr[scnd] && scnd >= c)
            {
                buff = arr[scnd];
                arr[scnd] = arr[c];
                arr[c] = buff;
                scnd++;
                QS(arr, frst, scnd);
            }*/
        }
        static void Fill(int[] arr)
        {
            Random rnd = new Random();
            for(int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(10);
            }
        }

        static void Write (int[] arr)
        {
            for(int j = 0; j < arr.Length; j++)
            {
                Console.WriteLine(arr[j]);
            }
        }

        static void Main(string[] args)
        {
            Console.Write("Укажите длину массива : ");
            int t = int.Parse(Console.ReadLine());
            int[] arr = new int[t];
            int frst = 0, scnd = arr.Length - 1;
            Fill(arr);

            Write(arr);
            Console.WriteLine("*****************************************");
            QS(arr, frst, scnd);
            Write(arr);

            Console.ReadKey();
        }
    }
}
