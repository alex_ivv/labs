﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            int c,t;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Random rnd = new Random();
            int g = rnd.Next(101);
            t = 0;
            Console.WriteLine("Угадай загаданное число");
            c = int.Parse(Console.ReadLine());
            do
            {
                t++;
                if (c < g)
                {
                    Console.WriteLine("Загаданное число больше");
                    c = int.Parse(Console.ReadLine());
                }
                else if (c > g)
                {
                    Console.WriteLine("Загаданное число меньше");
                    c = int.Parse(Console.ReadLine());
                }
                else
                {
                    Console.WriteLine("Ты угадал! Это было число : {0}", g);
                    c = int.Parse(Console.ReadLine());
                    sw.Stop();
                    Console.WriteLine("Затраченное время : {0}", sw.Elapsed);
                }
                
            }
            while (t < 9);
            Console.WriteLine("Ты не угадал! Это было число : {0} ", g);
            sw.Stop();
            Console.WriteLine("Затраченное время : {0}", sw.Elapsed);
            Console.ReadKey();
        }
    }
}
