﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            int t;
            int[] array = new int[10];
            Random rnd = new Random();

            for(int i = 0; i < 10; i++)
            {
                array[i] = rnd.Next(20);

                Console.WriteLine("Номер элемента {0} :  {1}", i, array[i]); 
            }

            Console.WriteLine("-");
            for (int i = 0; i < 10; i = i + 2)
            {
                int g = array[i + 1];
                array[i + 1] = array[i];
                array[i] = g;
                //Console.WriteLine("Номер элемента {0} :  {1}", i, array[i]);
                
            }
            for (int i = 0; i < 10; i ++)
            {
                Console.WriteLine("Номер элемента {0} :  {1}", i, array[i]);
            }
                Console.ReadKey();
        }
    }
}
