﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JumpS
{
    class Program
    {
        static int JS(int[] array, int max, int i)
        {
            int c = max;
            int min = 0;
            while (i > array[c])
            {
                c = c + max;

                if (c > array.Length - 1)
                {
                    c = array.Length - 1;
                    min = array.Length - 1 - max;
                    if (i > array[c])
                        return -1;
                }
            }
            while (i < array[c] && c > min)
            {
                c = c - 1;
            }
            if (i == array[c])
                return c;
            else
                return -1;
        }

        /*static int JumpSearch(int[] array, int i, int min, int max, int t)
        {
            int c = min + t*(max - min);

            

            if (c > array.Length - 1)
            {
                c = array.Length;
                max = c;
                min = max - (int)Math.Sqrt(array.Length);
                max = max - 1;
                return JumpSearch(array, i, min, max, t);

            if (i > array[max])
                    return -1;
                return JumpSearch(array, i, min, max, t);
            }

            else if (min == max)
            {
                return -1;
            }

            else if (i < array[c])
            {
                max = max - 1;

                if (i > array[max])
                    return -1;
                return JumpSearch(array, i, min, max, t);
            }

            else if (i == array[c])
                return c;

            else
            {
                
                min = min + t*(int)Math.Sqrt(array.Length);
                max = max + t*(int)Math.Sqrt(array.Length);
                t = 1;
                return JumpSearch(array, i, min, max, t);

            }   
        }*/

        static void Main(string[] args)
        {
            int i = int.Parse(Console.ReadLine());
            int[] array = new int[] { 1, 2, 3, 5, 7, 9, 11, 13, 15, 17, 19, 31 };
            int max = (int)Math.Sqrt(array.Length);

            Console.WriteLine(JS(array, max, i));
            Console.ReadKey();
        }
    }
}
