﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionExample
{
    class Program
    {
        static int SafeDivision(int a, int b)
        {
            try
            {
                if (b == 5)
                {
                    throw new CustomException();
                }

                return a / b;
            }
            catch (DivideByZeroException ex)
            {
                return 0;
            }
        }

        static void Main(string[] args)
        {
            int a, b;
            a = int.Parse(Console.ReadLine());
            b = int.Parse(Console.ReadLine());

            try
            {
                Console.WriteLine(a / b);
            }
            catch (DivideByZeroException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
            }

            Console.ReadKey();
        }
    }
}
