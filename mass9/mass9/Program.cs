﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mass9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите длину массива : ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("Введите число k : ");
            int p = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            Random rnd = new Random();

            for (int i = 0; i < n; i++)
            {
                array[i] = rnd.Next(20);
                Console.Write(" {0}",array[i]);

            }
            Console.WriteLine();
            Console.WriteLine("---------------------------------------------------------------------");
            int left = 0;
            int right = n - 1;

            do
            {
                for (int j = left; j < right; j++)
                {
                    if (array[j]%p > array[j + 1]%p)
                    {
                        int buf = array[j + 1];
                        array[j + 1] = array[j];
                        array[j] = buf;
                    }
                }
                right--;
                for (int k = right; k > left; k--)
                {
                    if (array[k]%p < array[k - 1]%p)
                    {
                        int buf = array[k - 1];
                        array[k - 1] = array[k];
                        array[k] = buf;
                    }
                }
                left++;
            } while (right - left > 1);

            for (int i = 0; i < n; i++)
                Console.Write(" {0}",array[i]);
            Console.ReadKey();
        }
    }
}
