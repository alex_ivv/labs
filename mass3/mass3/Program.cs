﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mass3
{
    class Program
    {
        static void Main(string[] args)
        {
            int max, min;

            Console.Write("Введите длину массива : ");
            int n = int.Parse(Console.ReadLine());
            double[] array = new double[n];

            Random rnd = new Random();
            for(int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(-20,20) + rnd.NextDouble();
            }
            
            min = 0;
            max = n-1;
            double sum = 0;

            for (int j = 0; j < array.Length; j++)
            {
                if (array[j] < array[min])
                    min = j;
                if (array[j] > array[max])
                    max = j;
                Console.WriteLine(array[j]);
            }

            if (min > max)
            {
                int buf = min;
                min = max;
                max = buf;
            }
                for (int k = min; k <= max; k++)
                {
                    sum = sum + array[k];
                }
                
            
            Console.WriteLine("Сумма элементов от {0} до {1} равна : {2}  ",min + 1,max + 1,sum);
            Console.ReadKey();
        }
    }
}
