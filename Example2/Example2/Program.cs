﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example2
{
    class Program
    {
        static int Factorial(int i)
        {
            if (i == 1)
                return 1;

            return i * Factorial(i - 1);
        }

        static int Fibbonachi(int i)
        {
            if (i == 1 || i == 2)
                return 1;

            return Fibbonachi(i - 1) + Fibbonachi(i - 2);
        }

        static void Nat(int i)
        {
            if (i == 1)
            {
                Console.Write(i);
                return;
            }

            Nat(i - 1);
            Console.Write(i);
             
        }

        static void Main(string[] args)
        {

            int i = int.Parse(Console.ReadLine());
            Nat(i);

            Console.ReadKey();
        }
    }
}
