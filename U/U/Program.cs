﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace U
{
    class Program
    {
        static void Func(int i)
        {
            if (i % 10 == i)
            {
                Console.Write($"{i}");
                return;
            }

            Console.Write($"{i % 10} ");
            Func(i / 10);
            return;
        }

        static void Main(string[] args)
        {
            int i;

            Console.WriteLine("Введите i ");
            i = int.Parse(Console.ReadLine());

            Func(i);

            Console.ReadKey();
        }
    }
}
