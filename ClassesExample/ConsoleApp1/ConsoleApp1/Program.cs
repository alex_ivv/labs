﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int i, f;
            System.Diagnostics.Stopwatch sw = new Stopwatch();
            sw.Start();

            Console.Write("Введите число : ");
            i = int.Parse(Console.ReadLine());
            f = i % 2;
           
                if (f > 0 || f < 0)
                {
                    Console.WriteLine("Число {0} нечетное", i);
                }
                else 
                {
                    Console.WriteLine("Число {0} четное", i);
                }
            sw.Stop();
            Console.WriteLine("Затраченное время : {0}", sw.Elapsed);
            Console.ReadKey();
           
        }
    }
}
