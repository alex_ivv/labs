﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesExample
{
    public class Circle : Figure
    {
        public Point A { get; set; }
        public double R { get; set; }
        
        public Circle()
        {
            A = new Point(0, 0);
            R = 1;
        }

        /// <summary>
        /// Задаёт круг с центром в заданной точке с заданным радиусом
        /// </summary>
        /// <param name="a">Центр круга</param>
        /// <param name="r">Радиус круга</param>
        /// <exception cref="ArgumentException"/>
        public Circle(Point a, double r)
        {
            A = a;

            if (r < 0)
                throw new ArgumentException();

            R = r;
        }

        public Circle(Circle r)
        {
            A = new Point(r.A);
            R = r.R;
        }

        public override double S()
        {
            return Math.PI * Math.Pow(R, 2);
        }

        public override double P()
        {
            return 2 * Math.PI * R;
        }
        public override string ToString()
        {
            return "Circle";
        }
    }
}
