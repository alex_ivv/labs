﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesExample
{
    class Program
    {
        static void Draw(IDrawable drawable)
        {

        }

        static void Main(string[] args)
        {
            Figure[] figures = new Figure[3];
            Point[] points = new Point[4];
            Random rnd = new Random();
            
            

            Console.WriteLine("Заданные точки :");

            for (int j = 0; j < points.Length; j++)
            {
                Point point = new Point(rnd.Next(-10, 10), rnd.Next(-10, 10));
                points[j] = point;
                
                Console.Write($"({points[j].X},{points[j].Y}) ");
            }
            Console.WriteLine();

            Point a = points[0];
            Point b = points[1];
            Point c = points[2];
            Point d = points[3];
            

            Console.WriteLine();

            for (int k = 0; k < points.Length; k++)
            {
                Quaters g = points[k].GetQuaterNumber();

                if (g == 0)
                    Console.WriteLine($"Точка ({points[k].X},{points[k].Y}) не лежит ни в одной из плоскостей ");

                else
                {
                    Console.Write($"Точка ({points[k].X},{points[k].Y}) лежит в плоскости № ");
                    Console.WriteLine(points[k].GetQuaterNumber());
                }
            }
            
            Console.WriteLine();
            try
            {
                figures[0] = new Circle(a, -1);
                figures[1] = new Triangle(a, b, c);
                figures[2] = new Rectangle(a, b, c, d);
                
                
            

            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Недопостумые значения для фигуры");
                Console.ReadKey();
                return;
                
            }
            
            for(int i = 0; i < figures.Length; i++)
            {
                Console.WriteLine($"Площадь фигуры {figures[i]} равна {figures[i].S()}");
                Console.WriteLine($"Периметр фигуры {figures[i]} равна {figures[i].P()} ");
                Console.WriteLine();
            }
            

            
            
            Console.ReadKey();
        }
    }
}
