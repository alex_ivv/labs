﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesExample
{
    public class Rectangle : Figure, IDrawable
    {
        public Point A { get; set; } 
        public Point B { get; set; }
        public Point C { get; set; }
        public Point D { get; set; }

        public double Height
        {
            get
            {
                return A.GetRangeTo(B);
            }
        }
        public double Width
        {
            get
            {
                return A.GetRangeTo(C);
            }
        }

        public Rectangle()
        {
            A = new Point(0, 0);
            B = new Point(1, 0);
            C = new Point(0, 1);
            D = new Point(1, 1);
        }

        public Rectangle(Point a, Point b, Point c, Point d)
        {
            

            A = a;
            B = b;
            C = c;
            D = d;
        }

        public Rectangle(Point a, double heigth, double width)
        {
            A = a;
            B = new Point(a.X + heigth, a.Y);
            C = new Point(a.X, a.Y + width);
            D = new Point(a.X + heigth, a.Y + width);
        }

        public Rectangle(Rectangle r)
        {
            A = new Point(r.A);
            B = new Point(r.B);
            C = new Point(r.C);
            D = new Point(r.D);
        }

        public override double S()
        {
            return Width * Height;
        }

        public override double P()
        {
            return (Width + Height) * 2;
        }

        public override string ToString()
        {
            return "Rectangle";
        }

        public void Draw()
        {
            throw new NotImplementedException();
        }
    }
}
