﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesExample
{
    public abstract class Figure
    {
        public abstract double S();
        public abstract double P();
    }
}
