﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesExample
{
    public class Triangle : Figure
    { 
        public Point A { get; set; }
        public Point B { get; set; }
        public Point C { get; set; }

        

        public Triangle()
        {
            A = new Point(0, 0);
            B = new Point(1, 1);
            C = new Point(2, 0);
        }

        public Triangle(Point a, Point b, Point c)
        {
           

            A = a;
            B = b;
            C = c;
        }

        public Triangle(Triangle r)
        {
            A = new Point(r.A);
            B = new Point(r.B);
            C = new Point(r.C);
        }

        public override double S()
        {
            double AB = A.GetRangeTo(B);
            double AC = A.GetRangeTo(C);
            double BC = B.GetRangeTo(C);
            double P2 = (AB + AC + BC)/2;
            return Math.Sqrt(P2 * (P2 - AB) * (P2 - AC) * (P2 - BC));
        }
        
        public override double P()
        {
            return A.GetRangeTo(B) + A.GetRangeTo(C) + B.GetRangeTo(C);
        }

        public override string ToString()
        {
            return "Triangle"; 
        }
    }
}
