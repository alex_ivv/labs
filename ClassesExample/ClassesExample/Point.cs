﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesExample
{
    public class Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point()
        {
            X = 0;
            Y = 0;
        }

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        public Point(Point p)
        {
            X = p.X;
            Y = p.Y;
        }


        /// <summary>
        /// Метод возвращает номер координатной четверти. Возвращает -1, если точка лежит на какой-нибудь оси
        /// </summary>
        /// <returns></returns>
        public Quaters GetQuaterNumber()
        {
            
            if (X == 0 || Y == 0)
                return Quaters.None;
  
            else if (X > 0 && Y > 0)
                return Quaters.First;

            else if (X < 0 && Y > 0)
                return Quaters.Second;

            else if (X < 0 && Y < 0)
                return Quaters.Third;

            else
                return Quaters.Forth;
        }

        public double GetRangeTo(Point r)
        {
            return Math.Sqrt(Math.Pow(X - r.X, 2) + Math.Pow(Y - r.Y, 2));
        }

        public override string ToString()
        {
            return "Point";
        }
    }
}
