﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mass6
{
    class Program
    {
        static void Main(string[] args)
        {
            int neg;

            Console.Write("Введите длину массива : ");
            int n = int.Parse(Console.ReadLine());
            double[] array = new double[n];

            Random rnd = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(-20, 20) + rnd.NextDouble();
            }
            
            neg = n - 1;
            double sum = 0;

            for (int j = array.Length; j >= 0 ; j--)
            {
                if (array[j] < 0)
                    neg = j; 
            }

            
            for (int k = neg; k < array.Length; k++)
            {
                sum = sum + Math.Abs(array[k]);
            }


            Console.WriteLine("Сумма элементов от {0} до {1} равна : {2}  ", neg + 1, n + 1, sum);
            Console.ReadKey();
        }
    }
}
